import React, { Component } from "react";
import TodoItems from "./TodoItems";
import './TodoList.css';


class TodoList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            items: []
        };

        this.addItem = this.addItem.bind(this);
    }

    addItem(e) {
        if (this._inputElement.value !== "") {
            var newItem = {
                text: this._inputElement.value,
                key: Date.now()
            };


            this.setState((prevState) => {
                return {
                    items: prevState.items.concat(newItem)
                };
            });

            this._inputElement.value = "";
        } else {
            alert('Favor inserir um texto');
        }

        console.log(this.state.items);
        e.preventDefault();
    }

    render() {
        return (
            <div className="todoListMain">
                <div>
                    <form onSubmit={this.addItem}>
                        <input placeholder="Enter task" ref={(a) => this._inputElement = a}></input>
                        <div className="btn-group">
                            <button type="submit">Adicionar</button>
                        </div>
                    </form>
                </div>
                <TodoItems entries={this.state.items} />
            </div>
        );
    }
}

export default TodoList;